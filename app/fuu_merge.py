#!/usr/bin/env python3

import urllib.request
import json
import csv
import sys


def import_csv_to_list(name: str) -> list:
    """Creates a list from a CSV file

    Args:
        name (str): The name of CSV file

    Returns:
        list: the original list
    """
    with open(name, newline='') as csv_file:
        reader = csv.reader(csv_file)
        data = list(reader)
        return data


def merge_list_with_api(data: list) -> list:
    """Merges two lists in a format proposed by FUU

    Args:
        data (list): a list with rows Account ID, Account Name, First Name, Created On

    Returns:
        list: a list with rows Account ID, First Name, Created On, Status, Status Set On
    """
    output = [["Account ID", "First Name", "Created On", "Status", "Status Set On"]]
    for i in data[1:]:
        j, k = process_json(i[0])
        output += [[i[0], i[2], i[3], j, k]]
    return output


def output_list_to_csv(name: str, data: list) -> None:
    """Writes a list to a CSV file

    Args:
        name (str): The name of CSV file
        data (list): The list to write
    """
    with open(name, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for i in data:
            writer.writerow(i)


def process_json(acn_id: str) -> tuple:
    """Processes the data from Restful Status API

    Args:
        acn_id (str): Account ID to query (actually an integer)

    Returns:
        tuple: a tuple with "status" and "created on" strings tied to relevant acn_id
    """
    with urllib.request.urlopen("http://interview.fuu.loc/v1/accounts/" + acn_id) as url:
        data = json.loads(url.read().decode())
    status = data["status"]
    created = data["created_on"]
    return status, created


if __name__ == '__main__':
    if len(sys.argv) > 2:
        fuu_list = import_csv_to_list(sys.argv[1])
        output = merge_list_with_api(fuu_list)
        output_list_to_csv(sys.argv[2], output)
    else:
        print("Usage: " + sys.argv[0] + " <input_file> <output_file>")
        exit(1)
