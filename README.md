# fuu_merge.py

A tool to merge CSV data from one place to another, making use of FUU "REST" API.

## Installation
Main application file resides in the app/ directory and can be set as executable.

The package uses fairly standard Python 3 modules. No magic is necessary to run. For development purposes a proper pipenv is required (or just use pytest with pytest-mock)

## Usage
./python3 fuu_merge.py input.csv output.csv

## Testing
As mentioned above, for test execution there is need to use pytest testing framerwork.

## Caveats
* This app disregards mixed order of CSV headers and will fail miserably when the input or API fields have been mixed
* It's also ineffective for large data sets
* It doesn't cover all test cases (this was supposed to be DevOps job FFS, though they mentioned QA capability)
* API URL is hardcoded and code doesn't handle endpoint issues

