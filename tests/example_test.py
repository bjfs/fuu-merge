import pytest
import app.fuu_merge as fuu_merge


@pytest.fixture()
def mocked_api(mocker):
    mocker.patch('app.fuu_merge.process_json', return_value=('collections','2015-08-08'))


def test_list_merge(mocked_api):
    assert fuu_merge.merge_list_with_api([["Account ID", "Account Name", "First Name", "Created On"],
                                          ['88888','dococt','Otto','2013-08-08']])\
           == [["Account ID", "First Name", "Created On", "Status", "Status Set On"],
               ['88888', 'Otto', '2013-08-08', 'collections', '2015-08-08']]


def test_import_csv_to_list_not_found_error():
    with pytest.raises(FileNotFoundError):
        fuu_merge.import_csv_to_list('')

